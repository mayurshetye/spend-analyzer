package com.gloresoft.spendanalyzer.repository;

import com.gloresoft.spendanalyzer.entities.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, String> {

}
