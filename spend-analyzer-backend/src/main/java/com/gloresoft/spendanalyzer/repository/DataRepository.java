package com.gloresoft.spendanalyzer.repository;

import com.gloresoft.spendanalyzer.entities.BillingData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface DataRepository extends JpaRepository<BillingData, Long> {
    List<BillingData> findByBookingDateBetween(Date from, Date to);

}
