package com.gloresoft.spendanalyzer.services.impl;

import com.gloresoft.spendanalyzer.entities.BillingData;
import com.gloresoft.spendanalyzer.repository.DataRepository;
import com.gloresoft.spendanalyzer.services.DataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class DataServiceImpl implements DataService {

    private final DataRepository dataRepository;

    public DataServiceImpl(DataRepository dataRepository){
        this.dataRepository = dataRepository;
    }

    @Override
    public List<BillingData> getAll() {
        return this.dataRepository.findAll();
    }

    @Override
    public void importData(List<BillingData> data) {
        UUID importId = generateImportId();
        data.forEach(entry -> entry.setImportId(importId));
        this.dataRepository.saveAll(data);
    }

    //TODO - Replace with Dto pattern and Mapping Entity annotation
    @Override
    public void update(BillingData billingData) {
        //BillingData billingDataInDb = this.dataRepository.findById(billingData.getId())
                                         //   .orElseThrow(IllegalArgumentException::new);
		/*
		 * billingDataInDb.setAmount(billingData.getAmount());
		 * billingDataInDb.setTag(billingData.getTag());
		 * billingDataInDb.setBookingDate(billingData.getBookingDate());
		 * billingDataInDb.setImportId(billingData.getImportId());
		 * billingDataInDb.setUsage(billingData.getUsage());
		 * billingDataInDb.setBeneficiaryOrPayer(billingData.getBeneficiaryOrPayer());
		 */

        this.dataRepository.save(billingData);

    }

    //TODO - Use batch update
    @Override
    public void bulkUpdate(List<BillingData> data) {
        data.forEach(this::update);
    }

    private UUID generateImportId() {
        return UUID.randomUUID();
    }
}
