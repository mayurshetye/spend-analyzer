package com.gloresoft.spendanalyzer.services;

import com.gloresoft.spendanalyzer.dto.Summary;

import java.util.Date;
import java.util.List;

public interface SummaryService {

    List<Summary> generate(Date from, Date to);
}
