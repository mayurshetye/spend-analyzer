package com.gloresoft.spendanalyzer.services.impl;

import com.gloresoft.spendanalyzer.dto.Summary;
import com.gloresoft.spendanalyzer.entities.BillingData;
import com.gloresoft.spendanalyzer.repository.DataRepository;
import com.gloresoft.spendanalyzer.services.SummaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


import static com.gloresoft.spendanalyzer.constants.Constants.NONE_TAG;

@Service
@Slf4j
public class SummaryServiceImpl implements SummaryService {

    private final DataRepository dataRepository;

    public SummaryServiceImpl(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    @Override
    public List<Summary> generate(Date from, Date to) {
        List<BillingData> data = this.dataRepository.findByBookingDateBetween(from, to);
        List<Summary> summaryList = new ArrayList<>();

        if(data == null || data.isEmpty())
            return summaryList;

        Map<String, Double> amountByTag = data.stream()
                .collect(Collectors.groupingBy(spendData -> spendData.getTag() != null ? spendData.getTag().getName(): NONE_TAG,
                        Collectors.summingDouble(entry -> Math.abs(entry.getAmount()))));

        Double totalAmountSpent = 0.0;

        for(Double amount: amountByTag.values()){
            totalAmountSpent += amount;
        }



        for(Map.Entry<String, Double> entry : amountByTag.entrySet()) {

            Double percentage = (entry.getValue()/totalAmountSpent)*100;

            Summary summary = new Summary();
            summary.setTag(entry.getKey());
            summary.setTotalAmount(entry.getValue());
            summary.setPercentage(percentage);

            summaryList.add(summary);
        }

        return summaryList;

    }
}
