package com.gloresoft.spendanalyzer.services.impl;

import com.gloresoft.spendanalyzer.entities.Tag;
import com.gloresoft.spendanalyzer.repository.TagRepository;
import com.gloresoft.spendanalyzer.services.TagService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    public TagServiceImpl(TagRepository tagRepository){
        this.tagRepository = tagRepository;
    }

    @Override
    public void create(String tagName) {
        Tag tag = new Tag();
        tag.setName(tagName);
        this.tagRepository.save(tag);
    }

    @Override
    public void delete(String tagName) {
        this.tagRepository.deleteById(tagName);
    }

    @Override
    public List<Tag> getAll() {
        return this.tagRepository.findAll();
    }
}
