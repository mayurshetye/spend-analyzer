package com.gloresoft.spendanalyzer.services;



import com.gloresoft.spendanalyzer.entities.BillingData;

import java.util.List;

public interface DataService {
    List<BillingData> getAll();

    void importData(List<BillingData> data);

    void update(BillingData billingData);

    void bulkUpdate(List<BillingData> data);
}
