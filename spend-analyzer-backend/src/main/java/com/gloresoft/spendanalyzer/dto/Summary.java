package com.gloresoft.spendanalyzer.dto;

import lombok.Data;


@Data
public class Summary {

    private String tag;

    private Double totalAmount;

    private Double percentage;
}
