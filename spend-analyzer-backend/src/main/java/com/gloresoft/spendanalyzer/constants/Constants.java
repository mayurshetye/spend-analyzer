package com.gloresoft.spendanalyzer.constants;

public class Constants {

    // CSV Headers
    public static final String BOOKING_DATE = "Buchungstag";
    public static final String USAGE = "Verwendungszweck";
    public static final String BENEFICIARY_OR_PAYER = "Beguenstigter/Zahlungspflichtiger";
    public static final String AMOUNT = "Betrag EUR";

    public static final String DATE_FORMAT = "dd.MM.yy";

    public static final String ENCODING = "UTF-8";

    public static final char DELIMITER = ';';

    public static final String NONE_TAG = "None";

}
