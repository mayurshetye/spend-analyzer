package com.gloresoft.spendanalyzer.util;

import com.gloresoft.spendanalyzer.entities.BillingData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.gloresoft.spendanalyzer.constants.Constants.*;

@Slf4j
public class CSVUtil {

    public static List<BillingData> csvToBillingData(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, ENCODING));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT
                             .withDelimiter(DELIMITER)
                             .withFirstRecordAsHeader()
                             .withIgnoreHeaderCase()
                             .withTrim())) {

            List<BillingData> billingDataList = new ArrayList<>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                BillingData billingData = new BillingData();
                billingData.setBeneficiaryOrPayer(csvRecord.get(BENEFICIARY_OR_PAYER));
                billingData.setUsage(csvRecord.get(USAGE));
                billingData.setBookingDate(parseDate(csvRecord.get(BOOKING_DATE)));
                billingData.setAmount(parseAmount(csvRecord.get(AMOUNT)));
                billingDataList.add(billingData);
            }

            return billingDataList;
        } catch (IOException | ParseException e) {
            throw new RuntimeException("Failed to parse CSV file: " + e.getMessage());
        }
    }

    private static Date parseDate(String dateStr) throws ParseException {
        log.debug("Parsing date {} to {} format", dateStr, DATE_FORMAT);
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);

        return df.parse(dateStr);
    }

    private static Double parseAmount(String amtStr) throws ParseException {
        NumberFormat format = NumberFormat.getInstance(Locale.GERMAN);
        Number number = format.parse(amtStr);
        return number.doubleValue();
    }
}
