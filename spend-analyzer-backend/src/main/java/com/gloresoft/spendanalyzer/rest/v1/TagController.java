package com.gloresoft.spendanalyzer.rest.v1;


import com.gloresoft.spendanalyzer.entities.Tag;
import com.gloresoft.spendanalyzer.services.TagService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/tags")
@Slf4j
public class TagController {

    @Autowired
    private TagService tagService;

    @GetMapping
    public List<Tag> getAllTags(){
        log.info("Fetching all tags");
        return this.tagService.getAll();
    }

    @PostMapping
    public void createTag(@RequestBody String tagName){
        log.info("Creating tag {}", tagName);
        this.tagService.create(tagName);
        log.info("Tag {} created", tagName);
    }

    @DeleteMapping("/{tagName}")
    public void deleteTag(@PathVariable String tagName){
        log.info("Deleting tag {}", tagName);
        this.tagService.delete(tagName);
        log.info("Tag {} deleted", tagName);
    }
}
