package com.gloresoft.spendanalyzer.rest.v1;

import com.gloresoft.spendanalyzer.dto.Summary;
import com.gloresoft.spendanalyzer.services.SummaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.gloresoft.spendanalyzer.constants.Constants.DATE_FORMAT;

@RestController
@RequestMapping("/v1/summary")
@Slf4j
public class SummaryController {

    @Autowired
    private SummaryService summaryService;

    @GetMapping("/{fromDate}/{toDate}")
    public List<Summary> getSummary(@PathVariable String fromDate,
                                    @PathVariable String toDate) throws ParseException {
        log.info("Generating spend summary from {} to {}", fromDate, toDate);
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        return this.summaryService.generate(df.parse(fromDate), df.parse(toDate));
    }
}
