package com.gloresoft.spendanalyzer.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
@Table(name = "spend_data", uniqueConstraints = @UniqueConstraint(columnNames = {"id"}))
public class BillingData {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "import_id")
    private UUID importId;

    @Column(name = "booking_date")
    @NotNull
    private Date bookingDate;

    private String usage;

    @Column(name = "beneficiary_or_Payer")
    private String beneficiaryOrPayer;

    @NotNull
    private Double amount;

//    @ManyToOne
  //  @JoinColumn(name = "tag")
   // private Tag tag;

    @ManyToOne
    @JoinColumn(name = "tag_name", referencedColumnName = "name")
    private Tag tag;
}
