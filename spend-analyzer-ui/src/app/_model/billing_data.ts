import { Tag } from "./tag";


export class BillingData {
  id: string | undefined;
  importId: string | undefined;
  bookingDate: Date | undefined;
  usage: string | undefined;
  beneficiaryOrPayer: string | undefined;
  amount: number | undefined;
  tag: Tag = new Tag();
}
