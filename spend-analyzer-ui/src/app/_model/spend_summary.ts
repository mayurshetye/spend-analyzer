export class SpendSummary {
    tag: string | undefined;
    totalAmount: number | undefined;
    percentage: number | undefined;

}
  