import { Component, OnInit, ViewChild } from '@angular/core';
import { BillingData } from "../_model/billing_data";
import { DataService } from '../_services/data.service';
import { Tag } from '../_model/tag';
import { TagService } from '../_services/tag.service';
import { MatTableDataSource } from '@angular/material/table';
import {FormControl} from '@angular/forms';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import {MatPaginator} from '@angular/material/paginator';

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment, Moment} from 'moment';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

// Workaround
// Possible bug
const DUMMY_DATE = new Date('01/01/2020');



@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})


export class DataComponent implements OnInit {
  billingData: BillingData[]| undefined;
  tags: Tag[]| undefined;
  displayedColumns: string[] = ['bookingDate', 'beneficiaryOrPayer', 'amount', 'usage', 'tags'];

  dataSource!: MatTableDataSource<BillingData>;
  month = new FormControl(moment());
  year = new FormControl(moment());

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  

  constructor(private dataService: DataService, private tagService: TagService) { }

  ngOnInit(): void {
    this.getBillingData();
    this.getTags();    
  }

  getBillingData() {
    this.dataService.getData()
      .subscribe((dataArr: BillingData[]) => {
          this.billingData = dataArr;
          this.dataSource = new MatTableDataSource(this.billingData);
          /* Filter predicate used for filtering table per different columns
          *  */
          this.dataSource.filterPredicate = this.getFilterPredicate();
          this.dataSource.paginator = this.paginator;
        },
        error => {  
          console.log('Error occurred while fetching billing data');
        }
      );
  }

  getTags(){
    this.tagService.getAllTags().subscribe((data: Tag[])=>  this.tags=data);
  }
  
  onValChange(value: any, billingData: BillingData){   
    billingData.tag= value;
    this.dataService.update(billingData).subscribe((event: any) => {
    });
  }

  /* this method well be called for each row in table  */
  getFilterPredicate() {
    return (row: BillingData, filters: string) => {
      // split string per '$' to array
      const filterArray = filters.split('$');
      const monthFilter = filterArray[0];
      const yearFilter = filterArray[1];
      
      const matchFilter = [];

      // Fetch data from row
      const columnBookingDate = row.bookingDate;

      if (columnBookingDate === undefined)
        return false;
      
      // verify fetching data by our searching values
      const customMonthFilter = (monthFilter === '') 
      || (new Date(columnBookingDate).getMonth() === new Date(monthFilter).getMonth()
          && new Date(columnBookingDate).getFullYear() === new Date(monthFilter).getFullYear());

      const customYearFilter = (yearFilter === '') 
      || new Date(columnBookingDate).getFullYear() === new Date(yearFilter).getFullYear();
      
      // push boolean values into array
      matchFilter.push(customMonthFilter);
      matchFilter.push(customYearFilter);

      // return true if all values in array is true
      // else return false
      return matchFilter.every(Boolean);
    };
  }

  applyFilter() {
    // create string of our searching values and split if by '$'
    const month = this.month.value.toDate();
    const year = this.year.value.toDate();

    const filterValue = 
    (month.getTime() === DUMMY_DATE.getTime() ? '' : month.toDateString())
    + 
    '$' 
    + 
    (year.getTime() === DUMMY_DATE.getTime() ? '': year.toDateString());


    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  chosenYearHandlerForMonthFilter(normalizedYear: Moment) {
    const ctrlValue = this.month.value;
    ctrlValue.year(normalizedYear.year());
    this.month.setValue(ctrlValue);
  }

  chosenMonthHandlerForMonthFilter(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.month.value;
    ctrlValue.month(normalizedMonth.month());
    this.month.setValue(ctrlValue);

    // Disable year filter
    this.year.setValue(moment(DUMMY_DATE));
    
    datepicker.close();
  }

  chosenYearHandlerForYearFilter(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.year.value;
    ctrlValue.year(normalizedYear.year());
    this.year.setValue(ctrlValue);

    // Disable month filter
    this.month.setValue(moment(DUMMY_DATE));

    datepicker.close();
  }
}
