import { Injectable } from '@angular/core';
import { 
  HttpEvent, HttpRequest, HttpHandler, 
  HttpInterceptor, HttpErrorResponse 
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, finalize } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(
      retry(1),
      catchError((error: HttpErrorResponse) => {
        if(error instanceof HttpErrorResponse){
          //Server 
          //alert(`HTTP Error: ${request.url}`);
          return throwError(error.message);
        } else{
          // Client Error
          return throwError(error);
        }
        }
      ),
      finalize(()=>{
        const profilingMsg = `${request.method} "${request.urlWithParams}"`;
        console.log("Profile log : " + profilingMsg);
      })
    );    
  }
}
