import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DataComponent} from './data/data.component';
import { FileComponent } from './file/file.component';
import { SummaryComponent } from './summary/summary.component';
import { TagComponent } from './tag/tag.component';

const routes: Routes = [
  { path: 'data-component', component: DataComponent },
  { path: 'summary-component', component: SummaryComponent },
  { path: 'tag-component', component: TagComponent },
  { path: 'file-component', component: FileComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
