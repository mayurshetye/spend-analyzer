
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Tag } from '../_model/tag';

@Injectable({
  providedIn: 'root'
})
export class TagService {


  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  add(tag: string): Observable<any>{
    return this.http.post(this.baseUrl + '/v1/tags', tag);
  }

  delete(tag: Tag): Observable<any> {
    let tagName = tag.name;
    let url = this.baseUrl + `/v1/tags/${tagName}`;
    return this.http.delete(url);
  }

  getAllTags(): Observable<Tag[]>{
    return this.http.get<Tag[]>(this.baseUrl + '/v1/tags');
  }

}
