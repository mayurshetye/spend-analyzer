import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";
import {BillingData} from "../_model/billing_data";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  billingData: BillingData | undefined;
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  // Returns an observable
  update(billingData: BillingData): Observable<any> {
    return this.http.put(this.baseUrl + '/v1/data', billingData);
  }

  getData(): Observable<BillingData[]>{
    return this.http.get<BillingData[]>(this.baseUrl + '/v1/data');
  }


}