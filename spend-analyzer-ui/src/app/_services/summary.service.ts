import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SpendSummary } from '../_model/spend_summary';

@Injectable({
  providedIn: 'root'
})
export class SummaryService {

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, public datepipe: DatePipe) { }

  generateSummary(fromDate: Date, toDate: Date): Observable<SpendSummary[]>{
    let fromDateStr =this.datepipe.transform(fromDate, 'dd.MM.yy');
    let toDateStr =this.datepipe.transform(toDate, 'dd.MM.yy');

    let url = this.baseUrl + `/v1/summary/${fromDateStr}/${toDateStr}`;

    return this.http.get<SpendSummary[]>(url);
  }
}
