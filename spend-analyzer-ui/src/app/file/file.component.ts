import { Component, OnInit } from '@angular/core';
import { FileService } from '../_services/file.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {
  file: File | null = null;
  submitted: boolean = false;
  uploadSuccessful: boolean = false;

  constructor(private fileService: FileService ) { }

  ngOnInit(): void {
    this.submitted = false;
  }

  onChange(event: any): void {
    this.file = event.target.files[0];
    this.submitted = false;
  }

  // OnClick of button Upload
  onUpload(): void {
    this.submitted = true;
    this.fileService.upload(this.file).subscribe(
       success => {
        this.uploadSuccessful = true;
      },
      error => {    
        this.uploadSuccessful = false;
      }
    );
  }

}
