import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Tag } from '../_model/tag';
import { TagService } from '../_services/tag.service';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

  inputTag: string = '';
  tags: Tag[] = [];
  errorMsg: string = ' ';

  constructor(private tagService: TagService) { }

  ngOnInit(): void {
    this.getAllTags();
    this.errorMsg = ' ';
  }

  add(tag: string): void {
    if (tag == undefined || tag == '' || tag == ' ') {
      this.errorMsg = 'Cannot add empty tag';
    }
    else{
      this.tagService.add(tag).subscribe(success =>{
        this.getAllTags();
        this.inputTag = ' ';
        this.errorMsg = ' ';
      },
      error => {
        this.errorMsg = 'Error occured while adding tag'
      });
    }
  }

  delete(tag: Tag): void {
    this.tagService.delete(tag).subscribe(success =>{
      this.getAllTags();
      this.errorMsg = ' ';
    },
    error => {
      this.errorMsg = 'Error occured while deleting tag';
    }); 
  }

  getAllTags(): void{
    this.tagService.getAllTags()
    .subscribe((tagArr: Tag[]) => {
        this.tags = tagArr;
        this.errorMsg = ' ';
      },
      error => {
        this.errorMsg = 'Some error occured';
      }
    );
  }

}
